$.trace.info(":KMEANS Server START:");

var gconn = {}; //Global DB gconnection

var output = "";

function callKMEANS() {

	var sql = "";
	var pcall;
	var rs;
	var sql_result;
	var html = "";
	var doit = true;

	if (doit) {

		sql = 'call "mta_kmeans.db::PAL_KMEANS" ("mta_kmeans.db::PAL_KMEANS_DATA_TBL","mta_kmeans.db::PAL_CONTROL_TBL",?, ?, ?, ?, ?)';

		html += "<pre>\n";
		html += "<p>" + sql  + "</p>\n";
		html += "</pre>\n";

		pcall = gconn.prepareCall(sql);

		if (pcall.execute()) {

			html += "<pre>\n";
			
			rs = pcall.getResultSet();
			var idx = 0;
			while (rs.next()) {
				idx++;
				html += "ASSIGNED: " + idx + "\n";
				html += "  ID: " + rs.getInteger(1) + "\n";
				html += "  CLUSTER: " + rs.getInteger(2) + "\n";
				html += "  DISTANCE: " + rs.getFloat(3) + "\n";
				html += "  SLIGHT_SILHOUETTE: " + rs.getFloat(3) + "\n";
				html += "  <br />\n";
			}

			html += "  \n";
			
			pcall.getMoreResults();

			rs = pcall.getResultSet();
			var idx = 0;
			while (rs.next()) {
				idx++;
				html += "CENTERS: " + idx + "\n";
				html += "  CLUSTER_ID: " + rs.getInteger(1) + "\n";
				html += "  V000: " + rs.getFloat(2) + "\n";
				html += "  V001: " + rs.getString(3) + "\n";
				html += "  V002: " + rs.getFloat(4) + "\n";
				html += "  \n";
			}

			html += "  \n";
			
			pcall.getMoreResults();

			rs = pcall.getResultSet();
			var idx = 0;
			while (rs.next()) {
				idx++;
				html += "SIL_CENTERS: " + idx + "\n";
				html += "  CLUSTER_ID: " + rs.getInteger(1) + "\n";
				html += "  SLIGHT_SILHOUETTE: " + rs.getFloat(2) + "\n";
				html += "  \n";
			}

			html += "  \n";
			
			pcall.getMoreResults();

			rs = pcall.getResultSet();
			var idx = 0;
			while (rs.next()) {
				idx++;
				html += "MODEL: " + idx + "\n";
				html += "  JID: " + rs.getInteger(1) + "\n";
				html += "  JSMODEL: " + rs.getString(2) + "\n";
				html += "  \n";
			}

			html += "  \n";
			
			pcall.getMoreResults();

			rs = pcall.getResultSet();
			var idx = 0;
			while (rs.next()) {
				idx++;
				html += "STATISTIC: " + idx + "\n";
				html += "  NAME: " + rs.getString(1) + "\n";
				html += "  VALUE: " + rs.getFloat(2) + "\n";
				html += "  \n";
			}

			html += "  \n";
			
			html += "</pre>\n";

		} else {
			html += "<strong>Failed to execute procedure</strong><br />\n";
		}

		pcall.close();
	}
	
	return(html);
}

try {
	// Always assume and exception can occur

	var request_method = $.request.method;
	var method = "";

	method += $.request.toString();

	switch (request_method) {
		case $.net.http.GET: // GET == 1
			method = "GET";
			break;
		case 2: // Unknown
			method = "Unknown";
			break;
		case $.net.http.POST: // POST = 3
			method = "POST";

			break;
		case $.net.http.PUT: // PUT = 4
			method = "PUT";
			break;
		case $.net.http.DEL: // DELETE = 5
			method = "DELETE";
			break;
		case 0: // OPTIONS
			method = "OPTIONS";
			break;
		case $.net.http.gconnECT: // gconnECT = 7
			method = "gconnECT";
			break;
		default:
			method = "Unhandled";
			break;
	}

	gconn = $.db.getConnection();

	output = callKMEANS();
	
	$.response.contentType = "text/html";
	$.response.setBody(output);

} catch (exception) {
	// Figure out what kind of exception it is and handle ones we care about.
	if (exception instanceof TypeError) {
		// Handle TypeError exception
		output += "TypeError: " + exception.toString() + "\n";
	} else if (exception instanceof ReferenceError) {
		// Handle ReferenceError
		output += "ReferenceError: " + exception.toString() + "\n";
	} else if (exception instanceof SyntaxError) {
		// Handle SyntaxError
		output += "SyntaxError: " + exception.toString() + "\n";
	} else {
		// Handle all other not handled exceptions
		output += "ExceptionError: " + exception.toString() + "\n";
	}

	$.response.contentType = "text/html";
	$.response.setBody(output);

} finally {
	// This code is always executed even if an exception is 
	if (gconn instanceof Object) {
		gconn.close();
	}
}

$.trace.info(":KMEANS Server END:");