/*eslint no-console: 0*/
"use strict";

var http = require("http");
var port = process.env.PORT || 3000;

var xsenv = require("sap-xsenv");

var hdb = require('hdb');

http.createServer(function(req, res) {

	var html = "";
	var html_start = '<html>\n';
	var html_head = '<head><title>KMEANS</title></head>\n';
	var html_body_start = '<body>\n';
	var html_body_end = '</body>\n';
	var html_end = '</html>\n';

	html += html_start;
	html += html_head;
	html += html_body_start;
	//html += html_body_end;
	//html += html_end;

	var output = "Something wicked this way comes...\n";

	if ((req.method === "GET") && (req.url === "/index.js")) { // Index.js

		var svcs = xsenv.getServices({
			hana: {
				tag: "hana"
			}
		});


		var hhost = svcs.hana.host;
		var hport = svcs.hana.port;
		
		var hschema = svcs.hana.schema;

		//var huser = svcs.hana.hdi_user;
		//var hpass = svcs.hana.hdi_password;
		var huser = svcs.hana.user;
		var hpass = svcs.hana.password;

		console.log('hhost:', hhost);
		console.log('hport:', hport);
		console.log('hschema:', hschema);
		console.log('huser:', huser);
		console.log('hpass:', hpass);


		var client = hdb.createClient({
			host: hhost,
			port: hport,
			user: huser,
			password: hpass
		});
		client.on('error', function(err) {
			console.error('Network connection error', err);
		});
		client.connect(function(err) {
			if (err) {
				return console.error('Connect error', err);
			}
			
			/*
			client.exec('CALL "mta_kmeans.db::PAL_KMEANS"("mta_kmeans.db::PAL_KMEANS_DATA_TBL","mta_kmeans.db::PAL_CONTROL_TBL",?,?,?,?,?)',
				function(err, rows) {
					client.end();
					if (err) {
						return console.error('Execute error:', err);
					}
					console.log('Results:', rows);
				});
			*/	
			var sql = 'call "' + hschema + '"."mta_kmeans.db::PAL_KMEANS" ("' + hschema + '"."mta_kmeans.db::PAL_KMEANS_DATA_TBL","' + hschema + '"."mta_kmeans.db::PAL_CONTROL_TBL",?, ?, ?, ?, ?)';
			console.log('sql:', sql);

			client.prepare(sql, function(perr, statement) {
				if (perr) {
					return console.error('Prepare error:', perr);
				}
				statement.exec(function(serr, T1, T2, T3, T4, T5) {
					if (serr) {
						return console.error('Exec error:', serr);
					}
					console.log('T1:', T1);
					console.log('T2:', T2);
					console.log('T3:', T3);
					console.log('T4:', T4);
					console.log('T5:', T5);
				});
			});
		});

		res.writeHead(200, {
			"Content-Type": "text/html"
		});

		html += '<p>INDEX.JS</p>\n';

		html += '<a href="kmeans.js">kmeans.js</a>\n';

		html += html_body_end;
		html += html_end;

		res.end(html);

	} else if ((req.method === "GET") && (req.url === "/kmeans.js")) { // KMEANS

		res.writeHead(200, {
			"Content-Type": "text/html"
		});

		html += '<p>KMEANS.JS</p>\n';

		html += '<a href="index.js">index.js</a>\n';

		html += html_body_end;
		html += html_end;

		res.end(html);

	} else if ((req.method === "GET") && (req.url === "/")) { // 

		res.writeHead(200, {
			"Content-Type": "text/html"
		});

		html += '<p>KMEANS NodeJS MTA</p>\n';

		html += '<a href="index.js">index.js</a>\n';
		html += '<br />\n';
		html += '<a href="kmeans.js">kmeans.js</a>\n';

		html += '<br />\n';
		html += '<br />\n';

		html +=
			'Based on this tutorial: <a href="http://www.sap.com/developer/tutorials/xsa-node-dbaccess.html">SAP HANA Database access from Node.js</a>\n';

		html += '<br />\n';

		html +=
			'PAL Doc: <a href="http://help.sap.com/saphelp_hanaplatform/helpdata/en/53/e6908794ce4bcaa440f5c4348f3d14/content.htm?frameset=/en/32/731a7719f14e488b1f4ab0afae995b/frameset.htm&current_toc=/en/32/731a7719f14e488b1f4ab0afae995b/plain.htm&node_id=22">KMEANS Algorithm</a>\n';

		html += html_body_end;
		html += html_end;

		res.end(html);

	} else { // Default
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});
		res.end(output);
	}

}).listen(port);

console.log("Server listening on port %d", port);